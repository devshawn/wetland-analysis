package com.devshawn.wetland.library.exception

class InputParseException extends RuntimeException {

    String message

    InputParseException() {
        this.message = "Please provide a valid input string. For no wetlands, use '{}'."
    }
}
