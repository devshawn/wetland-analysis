package com.devshawn.wetland.library.exception

class EmptyInputException extends RuntimeException {

    String message

    EmptyInputException() {
        this.message = "Please provide an input. To enter no wetland areas, use '{}'."
    }
}
