package com.devshawn.wetland.library.exception

import com.devshawn.wetland.library.domain.WetlandRectangle

class InvalidWetlandRectangleException extends RuntimeException {

    String message

    InvalidWetlandRectangleException(WetlandRectangle wetlandRectangle) {
        if (wetlandRectangle.bottomLeftPoint == null && wetlandRectangle.upperRightPoint == null) {
            this.message = "The wetland rectangle is invalid as both positions are null."
        } else if (wetlandRectangle.bottomLeftPoint == null) {
            this.message = "The wetland rectangle is invalid as the bottomLeftPoint is null."
        } else if (wetlandRectangle.upperRightPoint == null) {
            this.message = "The wetland rectangle is invalid as the upperRightPoint is null."
        } else {
            this.message = "A wetland rectangle is invalid as its position is outside of the farm."
        }
    }
}
