package com.devshawn.wetland.library.util

class WetlandUtil {

    static String formatFarmAsString(int[][] farm) {
        List<String> rows = farm.collect { it -> it.toString() }.reverse()
        return rows.inject('') { current, val -> "${current}${val}\n".toString() }
    }
}
