package com.devshawn.wetland.library.parser

import com.devshawn.wetland.library.domain.Position
import com.devshawn.wetland.library.domain.WetlandAnalysisRequest
import com.devshawn.wetland.library.domain.WetlandAnalysisResult
import com.devshawn.wetland.library.domain.WetlandRectangle
import com.devshawn.wetland.library.exception.EmptyInputException
import com.devshawn.wetland.library.exception.InputParseException

class WetlandAnalysisParser {

    static WetlandAnalysisRequest parseInput(String input) {
        if (!input) throw new EmptyInputException()
        String cleanedInput = removeFirstAndLastCharacter(input)
        if (!cleanedInput) return new WetlandAnalysisRequest([])
        List<WetlandRectangle> wetlandRectangles = parseRectangles(cleanedInput.split(',').toList())
        return new WetlandAnalysisRequest(wetlandRectangles)
    }

    static String unparseResult(WetlandAnalysisResult wetlandAnalysisResult) {
        if (wetlandAnalysisResult.result.size() == 0) {
            return 'There is no arable land. All land is wetland.'
        }
        return "${wetlandAnalysisResult.result.join(' ')}"
    }

    static List<WetlandRectangle> parseRectangles(List<String> rectangles) {
        try {
            return rectangles.collect { it ->
                List<String> coordinates = removeFirstAndLastCharacter(it).split(' ')
                return new WetlandRectangle(new Position(Integer.parseInt(coordinates[0]), Integer.parseInt(coordinates[1])), new Position(Integer.parseInt(coordinates[2]), Integer.parseInt(coordinates[3])))
            }
        } catch (NumberFormatException ex) {
            throw new InputParseException()
        }
    }

    static removeFirstAndLastCharacter(String str) {
        String trimmedStr = str?.trim()
        return (!trimmedStr || trimmedStr?.length() < 2) ? '' : trimmedStr.substring(1, trimmedStr.length() - 1)
    }

}
