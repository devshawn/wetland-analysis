package com.devshawn.wetland.library.enums

enum PositionShift {
    NONE,
    POSITIVE_X,
    POSITIVE_Y,
    NEGATIVE_X,
    NEGATIVE_Y
}
