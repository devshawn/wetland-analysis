package com.devshawn.wetland.library.domain

class WetlandAnalysisResult {

    List<Integer> result

    WetlandAnalysisResult(Map<Integer, Integer> sections) {
        this.result = sections.values().toList().sort()
    }
}
