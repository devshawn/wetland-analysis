package com.devshawn.wetland.library.domain

import com.devshawn.wetland.library.enums.PositionShift

class Position {

    int x

    int y

    Position(int x, int y) {
        this.x = x
        this.y = y
    }

    static generateShiftedPosition(int x, int y, PositionShift positionShift) {
        switch (positionShift) {
            case PositionShift.POSITIVE_X:
                return new Position(x + 1, y)
            case PositionShift.POSITIVE_Y:
                return new Position(x, y + 1)
            case PositionShift.NEGATIVE_X:
                return new Position(x - 1, y)
            case PositionShift.NEGATIVE_Y:
                return new Position(x, y - 1)
            case PositionShift.NONE:
                return new Position(x, y)
        }
    }
}
