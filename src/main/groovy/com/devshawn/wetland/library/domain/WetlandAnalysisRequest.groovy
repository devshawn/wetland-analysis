package com.devshawn.wetland.library.domain

import com.devshawn.wetland.library.constants.WetlandConstants

class WetlandAnalysisRequest {

    int width

    int height

    List<WetlandRectangle> wetlandRectangles

    WetlandAnalysisRequest(List<WetlandRectangle> wetlandRectangles) {
        this.wetlandRectangles = wetlandRectangles
        this.width = WetlandConstants.DEFAULT_WIDTH
        this.height = WetlandConstants.DEFAULT_HEIGHT
    }

    WetlandAnalysisRequest(List<WetlandRectangle> wetlandRectangles, int width, int height) {
        this.wetlandRectangles = wetlandRectangles
        this.width = width
        this.height = height
    }
}
