package com.devshawn.wetland.library.domain

class WetlandAnalysisCalculator {

    List<Position> queue = [] as LinkedList<Position>

    Map<Integer, Integer> sections = [:]

    int x = 0

    int y = 0

    int color = 1

    int upperBoundWidth

    WetlandAnalysisCalculator(int upperBoundWidth) {
        this.upperBoundWidth = upperBoundWidth
    }

    void updateCurrentPosition() {
        if (x == upperBoundWidth) {
            x = 0
            y++
        } else {
            x++
        }
    }

    void addToQueue(Position position) {
        queue.push(position)
    }

    boolean isQueueEmpty() {
        return queue.isEmpty()
    }

    Position popFromQueue() {
        return queue.pop()
    }

    void upsertSection(int key, int value) {
        sections.put(key, value)
    }

    void incrementColor() {
        color++
    }

    int fetchCurrentSection() {
        return sections.get(color)
    }
}
