package com.devshawn.wetland.library.domain

import com.devshawn.wetland.library.exception.InvalidWetlandRectangleException

class WetlandRectangle {

    Position bottomLeftPoint

    Position upperRightPoint

    WetlandRectangle(Position bottomLeftPoint, Position upperRightPoint) {
        this.bottomLeftPoint = bottomLeftPoint
        this.upperRightPoint = upperRightPoint
        if (bottomLeftPoint == null || upperRightPoint == null) {
            throw new InvalidWetlandRectangleException(this)
        }
    }
}
