package com.devshawn.wetland.library.analyzer

import com.devshawn.wetland.library.constants.WetlandConstants
import com.devshawn.wetland.library.domain.Position
import com.devshawn.wetland.library.domain.WetlandAnalysisCalculator
import com.devshawn.wetland.library.domain.WetlandAnalysisRequest
import com.devshawn.wetland.library.domain.WetlandAnalysisResult
import com.devshawn.wetland.library.domain.WetlandRectangle
import com.devshawn.wetland.library.enums.PositionShift
import com.devshawn.wetland.library.exception.InvalidWetlandRectangleException

class WetlandAnalyzer {

    int upperBoundWidth

    int upperBoundHeight

    int[][] farm

    WetlandAnalyzer(WetlandAnalysisRequest wetlandAnalysisRequest) {
        this.upperBoundWidth = wetlandAnalysisRequest.width - 1
        this.upperBoundHeight = wetlandAnalysisRequest.height - 1
        this.farm = new int[wetlandAnalysisRequest.width][wetlandAnalysisRequest.height]
        validateWetlandRectangles(wetlandAnalysisRequest.wetlandRectangles)
        initializeFarm()
        addWetlandRectanglesToFarm(wetlandAnalysisRequest.wetlandRectangles)
    }

    WetlandAnalysisResult analyze() {
        WetlandAnalysisCalculator calculator = new WetlandAnalysisCalculator(upperBoundWidth)

        while (isPositionInsideFarm(calculator.x, calculator.y)) {
            if (calculator.isQueueEmpty()) {
                if (isPositionUncoloredAndNotWetland(calculator.x, calculator.y)) {
                    calculator.incrementColor()
                    calculator.upsertSection(calculator.color, 0)
                    calculator.addToQueue(Position.generateShiftedPosition(calculator.x, calculator.y, PositionShift.NONE))
                }

                calculator.updateCurrentPosition()
            }

            while (!calculator.isQueueEmpty()) {
                Position position = calculator.popFromQueue()
                if (isPositionUncoloredAndNotWetland(position.x, position.y)) {
                    checkNearbyPositions(position.x, position.y, calculator)
                    colorPosition(position.x, position.y, calculator.color)
                    calculator.upsertSection(calculator.color, calculator.fetchCurrentSection() + 1)
                }
            }
        }

        return new WetlandAnalysisResult(calculator.sections)
    }

    void validateWetlandRectangles(List<WetlandRectangle> rectangles) {
        rectangles.each { rectangle -> validateWetlandRectangle(rectangle) }
    }

    void validateWetlandRectangle(WetlandRectangle rectangle) {
        boolean hasInvalidBottomLeftPositionX = rectangle.bottomLeftPoint.x < WetlandConstants.LOWER_BOUND_WIDTH || rectangle.bottomLeftPoint.x > upperBoundWidth
        boolean hasInvalidBottomLeftPositionY = rectangle.bottomLeftPoint.y < WetlandConstants.LOWER_BOUND_HEIGHT || rectangle.bottomLeftPoint.y > upperBoundHeight
        boolean hasInvalidUpperRightPositionX = rectangle.upperRightPoint.x < WetlandConstants.LOWER_BOUND_WIDTH || rectangle.upperRightPoint.x > upperBoundWidth
        boolean hasInvalidUpperRightPositionY = rectangle.upperRightPoint.y < WetlandConstants.LOWER_BOUND_HEIGHT || rectangle.upperRightPoint.y > upperBoundHeight
        if (hasInvalidBottomLeftPositionX || hasInvalidBottomLeftPositionY || hasInvalidUpperRightPositionX || hasInvalidUpperRightPositionY) {
            throw new InvalidWetlandRectangleException(rectangle)
        }
    }

    void initializeFarm() {
        (WetlandConstants.LOWER_BOUND_WIDTH..upperBoundWidth).each { x ->
            (WetlandConstants.LOWER_BOUND_HEIGHT..upperBoundHeight).each { y ->
                colorPosition(x, y, 0)
            }
        }
    }

    void addWetlandRectanglesToFarm(List<WetlandRectangle> wetlandRectangles) {
        wetlandRectangles.each { rectangle -> addWetlandRectangleToFarm(rectangle) }
    }

    void addWetlandRectangleToFarm(WetlandRectangle wetlandRectangle) {
        (wetlandRectangle.bottomLeftPoint.x..wetlandRectangle.upperRightPoint.x).each { x ->
            (wetlandRectangle.bottomLeftPoint.y..wetlandRectangle.upperRightPoint.y).each { y ->
                colorPosition(x, y, 1)
            }
        }
    }

    boolean isPositionInsideFarm(int x, int y) {
        return x <= upperBoundWidth && y <= upperBoundHeight
    }

    void checkNearbyPositions(int x, int y, WetlandAnalysisCalculator calculator) {
        if (x > 0) calculator.addToQueue(Position.generateShiftedPosition(x, y, PositionShift.NEGATIVE_X))
        if (x < upperBoundWidth) calculator.addToQueue(Position.generateShiftedPosition(x, y, PositionShift.POSITIVE_X))
        if (y > 0) calculator.addToQueue(Position.generateShiftedPosition(x, y, PositionShift.NEGATIVE_Y))
        if (y < upperBoundHeight) calculator.addToQueue(Position.generateShiftedPosition(x, y, PositionShift.POSITIVE_Y))
    }

    boolean isPositionUncoloredAndNotWetland(int x, int y) {
        return farm[x][y] == 0
    }

    void colorPosition(int x, int y, int color) {
        farm[x][y] = color
    }
}
