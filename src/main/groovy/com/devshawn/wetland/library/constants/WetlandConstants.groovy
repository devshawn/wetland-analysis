package com.devshawn.wetland.library.constants

class WetlandConstants {

    static final int LOWER_BOUND_WIDTH = 0

    static final int LOWER_BOUND_HEIGHT = 0

    static final int DEFAULT_WIDTH = 400

    static final int DEFAULT_HEIGHT = 600
}
