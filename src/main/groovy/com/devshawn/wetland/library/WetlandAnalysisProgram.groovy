package com.devshawn.wetland.library

import com.devshawn.wetland.library.analyzer.WetlandAnalyzer
import com.devshawn.wetland.library.domain.WetlandAnalysisRequest
import com.devshawn.wetland.library.domain.WetlandAnalysisResult
import com.devshawn.wetland.library.exception.EmptyInputException
import com.devshawn.wetland.library.exception.InputParseException
import com.devshawn.wetland.library.exception.InvalidWetlandRectangleException
import com.devshawn.wetland.library.parser.WetlandAnalysisParser

class WetlandAnalysisProgram {

    static void main(String[] args) {
        println 'Please enter your input... (example: {"0 292 399 307"})'
        String input = System.in.newReader().readLine()
        try {
            WetlandAnalysisRequest wetlandAnalysisRequest = WetlandAnalysisParser.parseInput(input)
            WetlandAnalyzer wetlandAnalyzer = new WetlandAnalyzer(wetlandAnalysisRequest)
            WetlandAnalysisResult wetlandAnalysisResult = wetlandAnalyzer.analyze()
            println WetlandAnalysisParser.unparseResult(wetlandAnalysisResult)
        } catch (EmptyInputException | InputParseException | InvalidWetlandRectangleException ex) {
            println ex.message
        }
    }
}
