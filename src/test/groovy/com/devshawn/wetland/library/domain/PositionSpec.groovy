package com.devshawn.wetland.library.domain

import spock.lang.Specification
import spock.lang.Unroll

@Unroll
class PositionSpec extends Specification {

    def 'constructor - (#x, #y)'() {
        when:
        Position result = new Position(x, y)

        then:
        result
        result.x == x
        result.y == y

        where:
        x      | y
        0      | 0
        1      | 2
        100043 | 5432
    }

    def 'no constructor exception on null - (#x, #y)'() {
        when:
        new Position(x, y)

        then:
        thrown(GroovyRuntimeException)

        where:
        x    | y
        null | null
        1    | null
        null | 1
    }
}
