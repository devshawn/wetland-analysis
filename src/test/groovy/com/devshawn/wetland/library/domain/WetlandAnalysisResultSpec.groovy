package com.devshawn.wetland.library.domain

import spock.lang.Specification

class WetlandAnalysisResultSpec extends Specification {

    def 'constructor - no items'() {
        when:
        WetlandAnalysisResult result = new WetlandAnalysisResult([:])

        then:
        result
        result.result.size() == 0
    }

    def 'constructor - sort items'() {
        when:
        WetlandAnalysisResult result = new WetlandAnalysisResult([2: 100, 3: 1, 4: 50000, 5: 4000, 6: 40])

        then:
        result
        result.result.size() == 5
        result.result[0] == 1
        result.result[1] == 40
        result.result[2] == 100
        result.result[3] == 4000
        result.result[4] == 50000
    }
}
