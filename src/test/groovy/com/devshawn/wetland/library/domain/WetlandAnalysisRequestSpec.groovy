package com.devshawn.wetland.library.domain

import com.devshawn.wetland.library.constants.WetlandConstants
import spock.lang.Specification
import spock.lang.Unroll

@Unroll
class WetlandAnalysisRequestSpec extends Specification {

    def 'constructor with default width and height - #wetlandRectangles'() {
        when:
        WetlandAnalysisRequest result = new WetlandAnalysisRequest(wetlandRectangles)

        then:
        result.wetlandRectangles == wetlandRectangles
        result.width == WetlandConstants.DEFAULT_WIDTH
        result.height == WetlandConstants.DEFAULT_HEIGHT

        where:
        _ | wetlandRectangles
        _ | [new WetlandRectangle(new Position(0, 0), new Position(1, 1))]
        _ | [new WetlandRectangle(new Position(0, 0), new Position(1, 1)), new WetlandRectangle(new Position(1, 6), new Position(6, 7))]
    }

    def 'constructor with width and height - #width - #height - #wetlandRectangles'() {
        when:
        WetlandAnalysisRequest result = new WetlandAnalysisRequest(wetlandRectangles)

        then:
        result.wetlandRectangles == wetlandRectangles
        result.width == WetlandConstants.DEFAULT_WIDTH
        result.height == WetlandConstants.DEFAULT_HEIGHT

        where:
        width | height | wetlandRectangles
        10    | 10     | [new WetlandRectangle(new Position(0, 0), new Position(1, 1))]
        4000  | 5000   | [new WetlandRectangle(new Position(0, 0), new Position(1, 1)), new WetlandRectangle(new Position(1, 6), new Position(6, 7))]
    }
}
