package com.devshawn.wetland.library.domain

import com.devshawn.wetland.library.exception.InvalidWetlandRectangleException
import spock.lang.Specification
import spock.lang.Unroll

@Unroll
class WetlandRectangleSpec extends Specification {

    def 'constructor - happy path - #bottomLeftPoint #upperRightPoint'() {
        when:
        WetlandRectangle result = new WetlandRectangle(bottomLeftPoint, upperRightPoint)

        then:
        result
        result.bottomLeftPoint == bottomLeftPoint
        result.upperRightPoint == upperRightPoint

        where:
        bottomLeftPoint            | upperRightPoint
        new Position(0, 0)         | new Position(1, 10)
        new Position(1000, 205023) | new Position(100000, 35325325)
    }

    def 'constructor - sad path - #bottomLeftPoint #upperRightPoint'() {
        when:
        new WetlandRectangle(bottomLeftPoint, upperRightPoint)

        then:
        InvalidWetlandRectangleException result = thrown(InvalidWetlandRectangleException)
        result.message == expectedMessage

        where:
        bottomLeftPoint    | upperRightPoint    | expectedMessage
        null               | null               | "The wetland rectangle is invalid as both positions are null."
        null               | new Position(0, 0) | "The wetland rectangle is invalid as the bottomLeftPoint is null."
        new Position(0, 0) | null               | "The wetland rectangle is invalid as the upperRightPoint is null."
    }
}
