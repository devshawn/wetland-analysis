package com.devshawn.wetland.library.util

import spock.lang.Specification
import spock.lang.Unroll

@Unroll
class WetlandUtilSpec extends Specification {

    def "test formatFarmAsString"() {
        setup:
        int[][] farm = new int[2][2]
        farm[0][0] = 1
        farm[0][1] = 2
        farm[1][0] = 3
        farm[1][1] = 4

        when:
        String result = WetlandUtil.formatFarmAsString(farm)

        then:
        result == "[3, 4]\n[1, 2]\n"
    }

    def "test formatFarmAsString with edge case inputs - #farm"() {
        when:
        String result = WetlandUtil.formatFarmAsString(farm)

        then:
        result == expectedResult

        where:
        farm          | expectedResult
        null          | ""
        new int[0][0] | ""
        new int[1][1] | "[0]\n"
        new int[1][2] | "[0, 0]\n"
        new int[2][1] | "[0]\n[0]\n"
    }
}
