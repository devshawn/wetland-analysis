package com.devshawn.wetland.library.parser

import com.devshawn.wetland.library.constants.WetlandConstants
import com.devshawn.wetland.library.domain.WetlandAnalysisRequest
import com.devshawn.wetland.library.domain.WetlandAnalysisResult
import com.devshawn.wetland.library.exception.EmptyInputException
import spock.lang.Specification
import spock.lang.Unroll

@Unroll
class WetlandAnalysisParserSpec extends Specification {

    def 'parseInput - null or empty string input - #input'() {
        when:
        WetlandAnalysisParser.parseInput(input)

        then:
        EmptyInputException result = thrown(EmptyInputException)
        result.message == "Please provide an input. To enter no wetland areas, use '{}'."

        where:
        _ | input
        _ | null
        _ | ''
    }

    def 'parseInput - empty wetlands {}'() {
        when:
        WetlandAnalysisRequest result = WetlandAnalysisParser.parseInput("{}")

        then:
        result
        result.wetlandRectangles.size() == 0
    }

    def 'parseInput - case 1'() {
        setup:
        String input = '{"0 292 399 307"}'

        when:
        WetlandAnalysisRequest result = WetlandAnalysisParser.parseInput(input)

        then:
        result
        result.wetlandRectangles.size() == 1
        result.wetlandRectangles[0].bottomLeftPoint.x == 0
        result.wetlandRectangles[0].bottomLeftPoint.y == 292
        result.wetlandRectangles[0].upperRightPoint.x == 399
        result.wetlandRectangles[0].upperRightPoint.y == 307
        result.width == WetlandConstants.DEFAULT_WIDTH
        result.height == WetlandConstants.DEFAULT_HEIGHT
    }

    def 'parseInput - case 2'() {
        setup:
        String input = '{"48 192 351 207", "48 392 351 407", "120 52 135 547", "260 52 275 547"}'

        when:
        WetlandAnalysisRequest result = WetlandAnalysisParser.parseInput(input)
        then:
        result
        result.wetlandRectangles.size() == 4
        result.wetlandRectangles[0].bottomLeftPoint.x == 48
        result.wetlandRectangles[0].bottomLeftPoint.y == 192
        result.wetlandRectangles[0].upperRightPoint.x == 351
        result.wetlandRectangles[0].upperRightPoint.y == 207
        result.wetlandRectangles[1].bottomLeftPoint.x == 48
        result.wetlandRectangles[1].bottomLeftPoint.y == 392
        result.wetlandRectangles[1].upperRightPoint.x == 351
        result.wetlandRectangles[1].upperRightPoint.y == 407
        result.wetlandRectangles[2].bottomLeftPoint.x == 120
        result.wetlandRectangles[2].bottomLeftPoint.y == 52
        result.wetlandRectangles[2].upperRightPoint.x == 135
        result.wetlandRectangles[2].upperRightPoint.y == 547
        result.wetlandRectangles[3].bottomLeftPoint.x == 260
        result.wetlandRectangles[3].bottomLeftPoint.y == 52
        result.wetlandRectangles[3].upperRightPoint.x == 275
        result.wetlandRectangles[3].upperRightPoint.y == 547
        result.width == WetlandConstants.DEFAULT_WIDTH
        result.height == WetlandConstants.DEFAULT_HEIGHT
    }

    def 'unparseResult - with values'() {
        setup:
        WetlandAnalysisResult wetlandAnalysisResult = new WetlandAnalysisResult([2: 35, 3: 1, 4: 3532523, 5: 3000])

        when:
        String result = WetlandAnalysisParser.unparseResult(wetlandAnalysisResult)

        then:
        result == "1 35 3000 3532523"
    }

    def 'unparseResult - no arable land'() {
        setup:
        WetlandAnalysisResult wetlandAnalysisResult = new WetlandAnalysisResult([:])

        when:
        String result = WetlandAnalysisParser.unparseResult(wetlandAnalysisResult)

        then:
        result == "There is no arable land. All land is wetland."
    }
}
