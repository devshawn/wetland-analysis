package com.devshawn.wetland.library.constants

import spock.lang.Specification

class WetlandConstantsSpec extends Specification {

    def 'constants have not changed'() {
        expect:
        WetlandConstants.LOWER_BOUND_WIDTH == 0
        WetlandConstants.LOWER_BOUND_HEIGHT == 0
        WetlandConstants.DEFAULT_WIDTH == 400
        WetlandConstants.DEFAULT_HEIGHT == 600
    }
}
