package com.devshawn.wetland.library

import com.devshawn.wetland.library.analyzer.WetlandAnalyzer
import com.devshawn.wetland.library.domain.Position
import com.devshawn.wetland.library.domain.WetlandAnalysisRequest
import com.devshawn.wetland.library.domain.WetlandAnalysisResult
import com.devshawn.wetland.library.domain.WetlandRectangle
import spock.lang.Specification

class WetlandSpec extends Specification {

    def 'return 0 when there is no arable land'() {
        setup:
        WetlandRectangle wetlandRectangle = new WetlandRectangle(new Position(0, 0), new Position(9, 9))
        WetlandAnalysisRequest wetlandAnalysisRequest = new WetlandAnalysisRequest([wetlandRectangle], 10, 10)
        WetlandAnalyzer wetlandAnalyzer = new WetlandAnalyzer(wetlandAnalysisRequest)

        when:
        WetlandAnalysisResult wetlandAnalysisResult = wetlandAnalyzer.analyze()

        then:
        wetlandAnalysisResult
        wetlandAnalysisResult.result.size() == 0
    }

    def 'simple wetland analysis with one wetland rectangle and one arable land'() {
        setup:
        WetlandRectangle wetlandRectangle = new WetlandRectangle(new Position(0, 0), new Position(4, 4))
        WetlandAnalysisRequest wetlandAnalysisRequest = new WetlandAnalysisRequest([wetlandRectangle], 10, 10)
        WetlandAnalyzer wetlandAnalyzer = new WetlandAnalyzer(wetlandAnalysisRequest)

        when:
        WetlandAnalysisResult wetlandAnalysisResult = wetlandAnalyzer.analyze()

        then:
        wetlandAnalysisResult
        wetlandAnalysisResult.result
        wetlandAnalysisResult.result.size() == 1
        wetlandAnalysisResult.result[0] == 75
    }
}
