package com.devshawn.wetland.library

import com.devshawn.wetland.library.analyzer.WetlandAnalyzer
import com.devshawn.wetland.library.domain.Position
import com.devshawn.wetland.library.domain.WetlandAnalysisRequest
import com.devshawn.wetland.library.domain.WetlandAnalysisResult
import com.devshawn.wetland.library.domain.WetlandRectangle
import spock.lang.Specification

class GivenInputWetlandSpec extends Specification {

    def "input 1"() {
        WetlandRectangle wetlandRectangle = new WetlandRectangle(new Position(0, 292), new Position(399, 307))
        WetlandAnalysisRequest wetlandAnalysisRequest = new WetlandAnalysisRequest([wetlandRectangle])
        WetlandAnalyzer wetlandAnalyzer = new WetlandAnalyzer(wetlandAnalysisRequest)

        when:
        WetlandAnalysisResult wetlandAnalysisResult = wetlandAnalyzer.analyze()

        then:
        wetlandAnalysisResult
        wetlandAnalysisResult.result
        wetlandAnalysisResult.result.size() == 2
        wetlandAnalysisResult.result[0] == 116800
        wetlandAnalysisResult.result[1] == 116800
        0 * _
    }

    def "input 2"() {
        WetlandRectangle wetlandRectangle1 = new WetlandRectangle(new Position(48, 192), new Position(351, 207))
        WetlandRectangle wetlandRectangle2 = new WetlandRectangle(new Position(48, 392), new Position(351, 407))
        WetlandRectangle wetlandRectangle3 = new WetlandRectangle(new Position(120, 52), new Position(135, 547))
        WetlandRectangle wetlandRectangle4 = new WetlandRectangle(new Position(260, 52), new Position(275, 547))
        WetlandAnalysisRequest wetlandAnalysisRequest = new WetlandAnalysisRequest([wetlandRectangle1, wetlandRectangle2, wetlandRectangle3, wetlandRectangle4])
        WetlandAnalyzer wetlandAnalyzer = new WetlandAnalyzer(wetlandAnalysisRequest)

        when:
        WetlandAnalysisResult wetlandAnalysisResult = wetlandAnalyzer.analyze()

        then:
        wetlandAnalysisResult
        wetlandAnalysisResult.result
        wetlandAnalysisResult.result.size() == 2
        wetlandAnalysisResult.result[0] == 22816
        wetlandAnalysisResult.result[1] == 192608
        0 * _
    }
}
