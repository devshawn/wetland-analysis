# Wetland Analysis

For this assessment, I chose to use Groovy and Gradle for development. It's what I use for most of my personal projects and what I use at work. I love that this is essentially a graph theory problem! I studied graph theory, mainly graph coloring, in both a directed study in mathematics and my senior seminar in computer science. I have a neat paper on [Conflict-Free Vertex Coloring of Planar Graphs][coloring], that focuses on a similar graph coloring problem.

## The Library & Program
I've structured the code to make it modular (so it could be a library, not just a interactive console program). I then included a parser and console program to allow standard input. In a real-world implementation of this, I would have also made a REST API to handle this. 

The problem is essentially a graph coloring problem. You can describe the farm and its land as a disconnected graph. The farm is stored as an `int[][]`, essentially a matrix. You can then color the areas of land (either wetland, or each section of arable land). I use a "queue" (using a LinkedList) to handle checking neighboring positions, and a map to store the area of each section of colored, arable land. Uncolored land is colored with a `0`, wetland is colored with a `1`, and then arable land sections are colored with a `2` or above. I implemented a breadth-first-search (BFS) algorithm to traverse the graph and calculate the sections of arable land.

### Running The Program

To run the program, you can use the included gradle wrapper and the `run` command. 

```
./gradlew run
```

The task will compile the groovy and then run the main program, printing:
```
Please enter your input... (example: {"0 292 399 307"})
```

You can then enter any input, such as the example given, by typing or pasting the input:
```
{"0 292 399 307"}
```

I've chosen to allow for an empty input of no wetlands by using `{}`. It has multiple input validation checks to validate the rectangles and the given input; though given the time constraints it likely could be improved (with more tests too!).

### Running Tests
I've written a number of unit and "integration" tests that test both individual functions and the overall program. These can be run by:

```
./gradlew test
```

## Next Steps
Going forward, I would finish writing unit tests for the smaller functions and add more overall "integration" tests besides the inputs that were given. This would also include adding more precise input validation as well as more null checks. I would also likely setup a Spring Boot server that would have a REST API for doing the wetland analysis. You could do cool things such as including a UI that allows easy input of wetlands and include a map of the farm, with regions colored as wetland or arable land. This server could then be built into a Docker image, deployed to the cloud as a single server or as separate front-end and back-end servers. Add in load balancers, a CDN, end-to-end tests, and you'd have a great start for an analysis web application.

[coloring]: https://digitalcommons.morris.umn.edu/horizons/vol4/iss2/8/